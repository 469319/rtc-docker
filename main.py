#!/usr/bin/python3
###################################################################
#
#  Alaska Satellite Facility DAAC
#
#  Sentinel Radiometric Terrain Correction using Sentinel Toolbox (SNAP)
#
###################################################################
#
# Copyright (C) 2016 Alaska Satellite Facility
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
#
###################################################################
import sys
import os
import re
import numpy
import datetime
import time
from optparse import OptionParser
import download_product
import rtc_utils
import logging as logger

logger.basicConfig(level=logger.DEBUG)

global baseSNAP
global input_file
global extDEM
global pixsiz

###################################################################
#  SET THIS VARIABLE
#
#  In order to run this program, you must set the variable baseSNAP
#  to point to the gpt executable on your system.
#
###################################################################
baseSNAP = '/usr/local/snap/bin/gpt '
###################################################################

###################################################################
#  Code setup section
###################################################################
usage = "usage: %prog [options] <S1A Zip File>"
parser = OptionParser(usage=usage)
parser.add_option("-r", dest="pixsiz", help="Pixel resolution - default = 10m", metavar="PS")
parser.add_option("-d", "--dem", dest="extDEM", help="External DEM file name", metavar="DEM")
parser.add_option("-c", "--clean", action="store_true", dest="cleanTemp", default=False,
                  help="Clean intermediate files")
(options, args) = parser.parse_args()

if (len(args) == 0):
    parser.print_help()
    print("")
    print("ERROR: No S1A zip file specified")
    print("")
    quit()

extDEM = options.extDEM
cleanTemp = options.cleanTemp
pixsiz = options.pixsiz

input_file = args[0]
product_identifier = re.split('/', input_file.replace('.zip', ''))[-1]

download_product.login_and_download_product(product_identifier)

cwdir = os.getcwd()
if (extDEM == None):
    print("No external DEM file specified")
if (extDEM != None):
    print("Using External DEM file %s" % extDEM)
    if "/" not in extDEM:
        extDEM = cwdir + "/" + extDEM
if "/" not in input_file:
    input_file = cwdir + "/" + input_file
if (pixsiz == None):
    pixsiz = 10.0

print("input_file = %s" % input_file)
print("BASENAME = %s" % product_identifier)
print("cwdir = %s" % cwdir)
print("DEM = %s" % extDEM)
print("CLEANUP = %s" % cleanTemp)
print("PIXEL SIZE = %s" % pixsiz)

###################################################################
#  Subroutine definitions
###################################################################
def timestamp(date):
    return time.mktime(date.timetuple())

# Create output directory
def createTempDir(cwdir, product_identifier):
    tempDir = f'{cwdir}/{product_identifier}'
    if not os.path.exists(cwdir):
        os.system(f'mkdir {cwdir}')
    if not os.path.exists(tempDir):
        os.system(f'mkdir {tempDir}')
    return tempDir


# Apply precise orbit file
def apply_orbit():
    start_time = datetime.datetime.now()
    cmd = f'{baseSNAP} Apply-Orbit-File -t {tempDir}/{product_identifier}_OB -PcontinueOnFail=\"true\" -PorbitType=\'Sentinel Precise (Auto Download)\' {input_file}'
    logger.info('Applying Precise Orbit file')
    logger.info(cmd)
    os.system(cmd)
    logger.info(f'Time to fix orbit: {timestamp(datetime.datetime.now()) - timestamp(start_time)}')
    return f'{product_identifier}_OB.dim'


# Apply thermal noise removal
def apply_TNR(inData):
    start_time = datetime.datetime.now()
    cmd = f'{baseSNAP} ThermalNoiseRemoval -t {tempDir}/{product_identifier}_OB_TNR -SsourceProduct={tempDir}/{inData}'
    logger.info('Applying Thermal Noise Removal')
    logger.info(cmd)
    os.system(cmd)
    logger.info(f'Time Thermal Noise Removal: {timestamp(datetime.datetime.now()) - timestamp(start_time)}')
    return f'{product_identifier}_OB_TNR.dim'

def apply_cal(inData):
    start_time = datetime.datetime.now()
    cmd = f'{baseSNAP} Calibration -PoutputBetaBand=true -PoutputSigmaBand=false -t {tempDir}/{product_identifier}_OB_TNR_CAL -Ssource={tempDir}/{inData}'
    logger.info('Applying Calibration')
    logger.info(cmd)
    os.system(cmd)
    logger.info(f'Time for Calibration: {timestamp(datetime.datetime.now()) - timestamp(start_time)}')
    return f'{product_identifier}_OB_TNR_CAL.dim'


# Apply terrain flattening
def apply_tf(inData):
    start_time = datetime.datetime.now()

    DEM = ' -PdemName=\"SRTM 1Sec HGT\"'
    if extDEM is not None:
        DEM = f' -PdemName=\"External DEM\" -PexternalDEMFile={extDEM} -PexternalDEMNoDataValue=0'

    cmd = f'{baseSNAP} Terrain-Flattening -t {tempDir}/{product_identifier}_OB_TNR_CAL_TF -Ssource={tempDir}/{inData} {DEM}'
    logger.info('Applying Terrain Flattening -- This will take some time')
    logger.info(cmd)
    os.system(cmd)
    logger.info(f'Time for Terrain Flattening: {timestamp(datetime.datetime.now()) - timestamp(start_time)}')
    return f'{product_identifier}_OB_TNR_CAL_TF.dim'


# Apply terrain correction
def apply_tc(inData):
    start_time = datetime.datetime.now()

    DEM = ' -PdemName=\"SRTM 1Sec HGT\"'
    if extDEM is not None:
        DEM = f' -PdemName=\"External DEM\" -PexternalDEMFile={extDEM} -PexternalDEMNoDataValue=0'

    if hemisphere == "S":
        map_projection = '-PmapProjection=EPSG:327%02d' % zone
    else:
        map_projection = '-PmapProjection=EPSG:326%02d' % zone

    cmd = f'{baseSNAP} Terrain-Correction -t {tempDir}/{product_identifier}_OB_TNR_CAL_TF_TC -Ssource={tempDir}/{inData} -PsaveDEM=true -PsaveProjectedLocalIncidenceAngle=true -PpixelSpacingInMeter={pixsiz} {map_projection} {DEM}'
    logger.info('Applying Terrain Correction -- This will take some time')
    logger.info(cmd)
    os.system(cmd)
    logger.info(f'Time for Terrain Correction: {timestamp(datetime.datetime.now()) - timestamp(start_time)}')
    return f'{product_identifier}_OB_TNR_CAL_TF_TC.dim'


# Apply linear to dB
def apply_dB(inData):
    start_time = datetime.datetime.now()
    cmd = f'{baseSNAP} LinearToFromdB -t {tempDir}/{product_identifier}_OB_TNR_CAL_TF_TC_dB -Ssource={tempDir}/{inData}'
    logger.info('Applying LinearToFromdB')
    logger.info(cmd)
    os.system(cmd)
    logger.info(f'Time for LinearToFromdB: {timestamp(datetime.datetime.now()) - timestamp(start_time)}')
    return f'{product_identifier}_OB_TNR_CAL_TF_TC_dB.dim'


# Get the UTM zone, central meridian, and hemisphere
def getZone(inData):
    temp = inData.replace('.zip', '.SAFE')
    if not os.path.isdir(temp):
        cmd = "unzip %s" % inData
        print(cmd)
        os.system(cmd)
    back = os.getcwd()
    os.chdir(temp)
    os.chdir('annotation')

    paths = os.listdir(os.getcwd())
    for temp in paths:
        if os.path.isfile(temp):
            toread = temp
            break
    f = open(toread, 'r')

    min_lon = 180
    max_lon = -180
    for line in f:
        m = re.search('<longitude>(.+?)</longitude>', line)
        if m:
            lon = float(m.group(1))
            if lon > max_lon:
                max_lon = lon
            if lon < min_lon:
                min_lon = lon
    f.close()
    print("Found max_lon of %s" % max_lon)
    print("Found min_lon of %s" % min_lon)
    center_lon = (float(min_lon) + float(max_lon)) / 2.0
    print("Found center_lon of %s" % center_lon)
    zone = int(float(lon)+180)/6 + 1
    print("Found UTM zone of %s" % zone)
    central_meridian = (zone-1)*6-180+3
    print("Found central meridian of %s" % central_meridian)

    f = open(toread, 'r')

    min_lat = 180
    max_lat = -180
    for line in f:
        m = re.search('<latitude>(.+?)</latitude>', line)
        if m:
            lat = float(m.group(1))
            if lat > max_lat:
                max_lat = lat
            if lat < min_lat:
                min_lat = lat
    f.close()
    print("Found max_lat of %s" % max_lat)
    print("Found min_lat of %s" % min_lat)
    center_lat = (float(min_lat) + float(max_lat)) / 2.0
    print("Found center_lat of %s" % center_lat)
    if (center_lat < 0):
        hemi = "S"
    else:
        hemi = "N"
    print("Found hemisphere of %s" % hemi)

    os.chdir(back)
    return zone, central_meridian, hemi

###################################################################
#  Start of main executable code
###################################################################
start = datetime.datetime.now()
tempDir = createTempDir(cwdir, product_identifier)
print("Processing in directory %s" % tempDir)

(zone, central_meridian, hemisphere) = getZone(input_file)

result = apply_orbit()
result = apply_TNR(result)
os.system(f'rm -r {tempDir}/*OB.data; rm {tempDir}/*OB.dim')
logger.info("removed *OB")
result = apply_cal(result)
os.system(f'rm -r {tempDir}/*TNR.data; rm {tempDir}/*TNR.dim')
result = apply_tf(result)
os.system(f'rm -r {tempDir}/*CAL.data; rm {tempDir}/*CAL.dim')
result = apply_tc(result)
os.system(f'rm -r {tempDir}/*TF.data; rm {tempDir}/*TF.dim')
result = apply_dB(result)
os.system(f'rm -r {tempDir}/*TC.data; rm {tempDir}/*TC.dim')

#
# Reformat files into GeoTIFFs
#
os.chdir(f'{tempDir}/{result.replace(".dim", ".data")}')
lasttime = datetime.datetime.now()


def createGeoTIFFs(aaaa):
    input_files = ['Gamma0_VV_db.img', 'Gamma0_VH_db.img', 'Gamma0_HH_db.img', 'Gamma0_HV_db.img', 'projectedLocalIncidenceAngle_db.img', 'elevation_db.img']
    for filename in input_files:
        if os.path.isfile(filename):
            filename_out = filename.replace(".img", ".tif")

            translate_cmd = f'gdal_translate -of GTiff {filename} {filename_out}'
            logger.info(f'{translate_cmd}')
            os.system(translate_cmd)

            move_cmd = f'mv {filename_out} ../{aaaa.replace(".dim", f"_{filename_out}")}'
            logger.info(f'{move_cmd}')
            os.system(move_cmd)
        else:
            logger.info(f'Skipping {filename} (file not found)')


rtc_utils.log_and_print_files_and_directories()

createGeoTIFFs(result)

print('Time to export: ', end='')
print(timestamp(datetime.datetime.now())-timestamp(lasttime))

print('Total processing time: ', end='')
print(timestamp(datetime.datetime.now())-timestamp(start))

os.chdir('..')
os.system('rm -r *.data; rm *.dim')

# rtc_utils.zip_processed_files(product_identifier)