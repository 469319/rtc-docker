import zipfile
import logging as logger
import os

APPLIED_STEPS_SUFFIX = "_OB_TNR_CAL_TF_TC_dB"


def zip_processed_files(identifier):
    output_filename = f"{identifier}_processed.zip"
    # TODO add more files, HH polarization for example
    filenames = [
        f"{identifier}{APPLIED_STEPS_SUFFIX}_GVH.tif",
        f"{identifier}{APPLIED_STEPS_SUFFIX}_GVV.tif",
        f"{identifier}{APPLIED_STEPS_SUFFIX}_INC.tif",
    ]

    with zipfile.ZipFile(output_filename, 'w') as zipf:
        for file in filenames:
            logger.info(f"Adding {file} into {output_filename}")
            zipf.write(file, arcname=file)

    logger.info(f"Archive {output_filename} has been created.")


def log_and_print_files_and_directories():
    cwd = os.getcwd()
    logger.info("================================================================")
    logger.info(f"Current Working Directory: {cwd}")
    items = os.listdir(cwd)
    for item in items:
        if os.path.isdir(item):
            logger.info(f"Directory: {item}")
        else:
            logger.info(f"File: {item}")
    logger.info("================================================================")